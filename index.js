// First, we load the expressjs module into our application and save it in a variable called express.
const express = require('express');

// Create an application with expressjs.
// This creates an application that uses express and stores it as app.
// app is our server.
const app = express();

// port is a variable that contains the port number we want to designate to our server.
const port = 4000;

//middleware
// express.json() is a method from express which allows us to handle the streaming of data and automatically parses the incoming JSON from our req body.
// app.use is used to run a method or another function for our expressjs api.
app.use(express.json());

let users = [
	{
		username : "BMadrigal",
		email : "fateReader@gmail.com",
		password : "weDontTalkAboutMe"
	},
	{
		username : "LuisaMadrigal",
		email : "strongSis@gmail.com",
		password : "pressure"
	}
];

let items = [
	{
		name : "roses",
		price : 170,
		isActive : true
	},
	{
		name : "tulips",
		price : 250,
		isActive : true
	}
];

// Express has methods to use as routes corresponding to HTTP methods.
// app.get(<endpoint>, <function hadnling req and res)

app.get('/', (req, res) => {

	// Once the route is accessed, we can send a response with the use of res.send().
	// res.send() actually combines writeHead() and end().
		// This is used to send a response to the client and end the request.
	res.send('Hello from ExpressJS Api!');
});

app.get('/greeting', (req, res) => {
	res.send('Hello from Batch169-Ayers!');
});

app.get('/users', (req, res) => {
	// res.send() stringifies the argument for you.
	res.send(users);
});

// How do we get data from the client as a request body?

app.post('/users', (req, res) => {

	console.log(req.body);

	let newUser = {
		username : req.body.username,
		email : req.body.email,
		password : req.body.password
	}

	users.push(newUser);
	console.log(newUser);

	res.send(users);
});

app.delete('/users', (req, res) => {
	users.pop();
	console.log(users);
	res.send(users)
});

// updating users route
// (:index (:id when connected to a database) - This is called a wildcard.)
// (You have to remove the : and add a number in place of the index.)

app.put('/users/:index', (req, res) => {

	// req.body - This will contain the updated password.
	console.log(req.body);

	// req.params object which contains the value in the url params
	// url params is captured by route parameter (:parameterName) and saved as property in req.params.
	console.log(req.params);

	// parseInt - the value of the number coming from req.params
	let index = parseInt(req.params.index);

	// Gets the user that we want to update with our index number from url params.
	users[index].password = req.body.password;

	// Sends the updated user to the client.
	// Provides the index variable to be the index for the particular item in the array.
	res.send(users[index]);
});

app.get('/users/getSingleUser/:index', (req, res) => {

	let index = parseInt(req.params.index);

	res.send(users[index]);
});

// Mini-Activity

app.get('/items', (req, res) => {
	res.send(items);
})

app.post('/items', (req, res) => {

	let newItem = {
		name : req.body.name,
		price : req.body.price,
		isActive : req.body.isActive
	}
	items.push(newItem);
	res.send(items);
})

app.put('/items/:index', (req, res) => {

	let index = parseInt(req.params.index);

	items[index].price = req.body.price;

	res.send(items[index]);
});



// A C T I V I T Y

app.get('/items/getSingleItem/:index', (req, res) => {
	let index = parseInt(req.params.index);
	res.send(items[index]);
});

app.put("/items/archive/:index", (req, res) => {
	let index = parseInt(req.params.index);
	items[index].isActive = false;
	res.send(items[index]);
});

app.put("/items/activate/:index", (req, res) => {
	let index = parseInt(req.params.index);
	items[index].isActive = true;
	res.send(items[index]);
});

// listen method - The server listens to the assigned port.
app.listen(port, () => console.log(`Server is running at port ${port}.`))